package edu.fridayjava2.common.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class SysLoginService {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    TokenService tokenService;

    public String login(String username,String password){
        //如何在login（）方法中让Security框架起作用
        //在方法中直接引入authenticationManager.authenticate(Authentication authentication),
        // 代码就会跳转去做认证
        //这里我们用UsernamePasswordAuthenticationToken作为我们的Authentication
        //UsernamePasswordAuthenticationToken通常需用用户名和密码作为参数
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,password));
        //怎么得到登录用户的信息，通过已经认证的authentication对象就能得到登录用户的信息
        LoginUser loginUser =(LoginUser) authentication.getPrincipal();
        String token = tokenService.createToken(loginUser);
        //登录认证成功之后，生成认证的Token，把Token返回给浏览器
        //下一次再发送请求时，请求中会带上Token，Security框架会分析Token的数据，
        // 然后如果是已经登录的用户，就不用再登录
        //如果是没有登录认证的用户，就需要登录。
        return token;
    }
}
