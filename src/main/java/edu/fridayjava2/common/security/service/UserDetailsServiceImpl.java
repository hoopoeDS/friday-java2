package edu.fridayjava2.common.security.service;

import edu.fridayjava2.model.SysUser;
import edu.fridayjava2.model.vo.SysUserVO;
import edu.fridayjava2.service.SysRoleService;
import edu.fridayjava2.service.SysUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * UserDetailsService要实现查询用户的基本信息+用户的权限信息
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    SysUserService sysUserService;
    @Autowired
    SysPermissionService sysPermissionService;

    //实现得到用户的完整信息（用户的基本信息+用户的权限信息）
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //用户的基本信息
        SysUser user = sysUserService.selectUserByUsername(username);
        return createLoginUser(user);
    }
     public UserDetails createLoginUser(SysUser user){
        //数据库实体类转换成视图层实体类
         SysUserVO userVO = new SysUserVO();
         BeanUtils.copyProperties(user,userVO);
         //用户的权限信息:sysPermissionService.getMenuPermissionByUserId(userVO)
        return new LoginUser(userVO,sysPermissionService.getMenuPermissionByUserId(userVO));
     }
}
