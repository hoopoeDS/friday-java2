package edu.fridayjava2.common.security.service;

import edu.fridayjava2.model.SysRole;
import edu.fridayjava2.model.vo.SysUserVO;
import edu.fridayjava2.service.SysMenuService;
import edu.fridayjava2.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
@Component
public class SysPermissionService {
    @Autowired
    SysRoleService sysRoleService;
    @Autowired
    SysMenuService sysMenuService;

    //通过用户的ID得到权限信息
    public Set<String> getRolePermissionByUserId(SysUserVO userVO){
        return sysRoleService.selectRolePermsByUserId(userVO.getUserId());
    }

    //通过用户ID得到菜单表中的perms
    public Set<String> getMenuPermissionByUserId(SysUserVO userVO){
        return sysMenuService.selectMenuPermissionByUserId(userVO.getUserId());
    }
}
