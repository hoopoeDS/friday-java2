package edu.fridayjava2.common.security.handle;

import com.alibaba.fastjson.JSON;
import edu.fridayjava2.common.constant.HttpStatus;
import edu.fridayjava2.common.result.RestResult;
import edu.fridayjava2.utils.StringUtils;
import edu.fridayjava2.utils.http.ServletUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        int code = HttpStatus.UNAUTHORIZED;
        //String msg = "认证失败";
        //RestResult.error(code,msg);
//        response.setCharacterEncoding("utf-8");
//        response.getWriter().println(RestResult.error(code,msg));
        String msg = StringUtils.format("请求：{},认证失败，可惜",request.getRequestURI());
        ServletUtils.renderString(response, JSON.toJSONString(RestResult.error(code,msg)));
    }
}
