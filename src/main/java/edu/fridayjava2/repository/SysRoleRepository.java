package edu.fridayjava2.repository;

import edu.fridayjava2.model.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

//跟角色相关的代码
public interface SysRoleRepository  extends JpaRepository<SysRole,Long> {
    //（2）通过用户ID查询用户对应的角色信息(指角色的所有信息)
    @Query(value="select * from sys_role where role_id in (select role_id from sys_user_role where user_id=:userIds)",nativeQuery=true)
    List<SysRole> selectRolePermsByUserId(@Param("userIds") Long userId);

    @Query(value="select * from sys_role where role_id in (select role_id from sys_user_role where user_id=:userIds)",nativeQuery=true)
    List<SysRole> selectRoleByUserId(@Param("userIds") Long userId);
}
