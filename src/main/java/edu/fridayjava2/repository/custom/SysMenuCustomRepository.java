package edu.fridayjava2.repository.custom;

import edu.fridayjava2.model.SysMenu;

import java.util.List;

public interface SysMenuCustomRepository {
    //通过用户ID查询菜单列表
    List<SysMenu> selectMenuListByUserId(SysMenu sysMenu, Long userId);
    //通过角色ID查询菜单列表
    List<SysMenu> selectMenuListByRoleId(Long roleId);
}
