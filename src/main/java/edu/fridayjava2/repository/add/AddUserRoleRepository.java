package edu.fridayjava2.repository.add;

import org.springframework.stereotype.Repository;

@Repository
public interface AddUserRoleRepository {
    int addUserRole(Long userId,Long[] roleIds);
}
