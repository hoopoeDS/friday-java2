package edu.fridayjava2.repository.add.impl;

import edu.fridayjava2.repository.add.AddUserRoleRepository;
import edu.fridayjava2.utils.SqlUtil;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class AddUserRoleRepositoryImpl implements AddUserRoleRepository {
    @PersistenceContext
    EntityManager entityManager;//实体类管理器，用于完成数据库操作
    @Override
    //把userid和roleid存到sys_user_role
    //Insert  into sys_user_role (user_id,role_id)  values (?,?),(?,?),(?,?);
    //用EntityManager生成Query，然后Query执行数据代码，完成数据库操作
    @Transactional
    public int addUserRole(Long userId,Long[] roleIds) {
        //第一步：拼接SQL代码
        StringBuffer sql = new StringBuffer();
        sql.append("insert  into sys_user_role (user_id,role_id)  values");
        //要拼接“(?,?),(?,?),(?,?)”，我们要弄清楚两个内容：
        //1、每一组有几个参数？2
        //2、有多少组？要取决于角色id的多少
        sql.append(SqlUtil.getBatchInsertSqlStr(roleIds.length,2));
        //第二步：生成Query对象
        Query query = entityManager.createNativeQuery(sql.toString());
        //setParameter(int i,Object s)  int i表示数据库代码中第几个问号，默认从1开始，Object s表示具体的值
        //设置SQL的值
        int index =1;
        for(int i=0;i<roleIds.length;i++){
            query.setParameter(index++,userId);
            query.setParameter(index++,roleIds[i]);
        }
        return query.executeUpdate();
    }
}
