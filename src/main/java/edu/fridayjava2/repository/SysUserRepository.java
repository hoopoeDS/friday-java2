package edu.fridayjava2.repository;

import edu.fridayjava2.model.SysUser;
import edu.fridayjava2.repository.add.AddUserRoleRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SysUserRepository  extends JpaRepository<SysUser,Long>, AddUserRoleRepository {

    //把del_flag设置为2.
    //SQL的更新语句： update 表名 set 字段 = 值 where 条件;  把用户id为100,101的del_flag设置为2
    @Query(value="update sys_user set del_flag = '2' where user_id in :userId ",nativeQuery=true)
    @Modifying
    int deleteUser(@Param("userId") Long[] userIds);

    //删除用户-角色信息
    @Query(value="Delete from sys_user_role where user_id = :userId",nativeQuery=true)
    @Modifying
    int deleteUserRole(@Param("userId")Long userId);

    //（1）	通过用户ID查询用户的基本信息
    //通过用户名得到用户
    @Query(value="select * from sys_user where del_flag=0 and user_name = :Username",nativeQuery=true)
    SysUser selectUserByUsername(@Param("Username") String username);




}
