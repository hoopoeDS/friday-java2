package edu.fridayjava2.controller;

import edu.fridayjava2.common.constant.Constants;
import edu.fridayjava2.common.result.RestResult;
import edu.fridayjava2.common.security.service.LoginUser;
import edu.fridayjava2.common.security.service.SysLoginService;
import edu.fridayjava2.common.security.service.SysPermissionService;
import edu.fridayjava2.common.security.service.TokenService;
import edu.fridayjava2.model.vo.SysMenuVO;
import edu.fridayjava2.model.vo.SysUserVO;
import edu.fridayjava2.service.SysMenuService;
import edu.fridayjava2.utils.http.ServletUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
public class SysLoginController {
    @Autowired
    SysLoginService loginService;
    @Autowired
    TokenService tokenService;
    @Autowired
    SysPermissionService permissionService;
    @Autowired
    SysMenuService menuService;
    //让登录请求需要去认证
    @RequestMapping("/login")
    public RestResult login(String username, String password){
        String token = loginService.login(username,password);
        //登录认证成功之后，生成认证的Token，把Token返回给浏览器
        //下一次再发送请求时，请求中会带上Token，Security框架会分析Token的数据，
        // 然后如果是已经登录的用户，就不用再登录
        //如果是没有登录认证的用户，就需要登录。
        RestResult result = RestResult.success();
        result.put(Constants.TOKEN,token);
        result.put("shenmenniuma",110022010);
        return result;
    }

    //获取登录用户授权信息
    @GetMapping("/getInfo")
    public RestResult getInfo(){
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUserVO user = loginUser.getUser();
        Set<String> roles = permissionService.getRolePermissionByUserId(user);
        Set<String> permissions = permissionService.getMenuPermissionByUserId(user);
        RestResult ajax = RestResult.success();
        ajax.put("user",user);
        ajax.put("roles",roles);
        ajax.put("permissions",permissions);
        return ajax;
    }
    //获取菜单路由信息
    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public RestResult getRouters() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 用户信息
        SysUserVO user = loginUser.getUser();
        List<SysMenuVO> menus = menuService.selectMenuTreeByUserId(user.getUserId());
        return RestResult.success(menuService.buildMenus(menus));
    }
}
