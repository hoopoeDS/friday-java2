package edu.fridayjava2.controller;

import edu.fridayjava2.dao.UserDao;
import edu.fridayjava2.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    @Autowired
    UserDao userDao;

    @RequestMapping("test")
    public String test(){
        return "index";
    }

    //测试方法二：通过@Query实现删除
    @RequestMapping("delete")
    @Transactional
    public String deleteTest(){
        userDao.deleteUserByUserName("ds");
        if(userDao.existsById(new Long(170))){
            return "删除失败";
        }else{
            return "删除成功";
        }
    }

    //测试方法一：通过JPA提供的方法实现删除
    @RequestMapping("delete2")
    @Transactional
    public String deleteTest2(){

        SysUser user = new SysUser();
        user.setUserId(new Long(170));
        user.setUserName("ds");
        user.setNickName("hhh");

        userDao.delete(user);

        if(userDao.existsById(new Long(170))){
            return "删除失败";
        }else{
            return "删除成功";
        }
    }

    //通过JPA方式实现修改用户操作
    @RequestMapping("save")
    @Transactional
    public String updateUser(){
        SysUser user = new SysUser();
        user.setUserId(new Long(134));
        user.setUserName("10112");
        user.setNickName("110110");

        userDao.save(user);

        return "success";

    }


//    /system/user/list
//    /system/user/add
//
//    @XXXXXXMapping
//
//    @RequestMapping
//
//    @DeleteMapping
//    @PutMapping
//    @PostMapping
//    @GetMapping

}
