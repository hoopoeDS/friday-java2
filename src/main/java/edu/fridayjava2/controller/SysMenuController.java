package edu.fridayjava2.controller;

import edu.fridayjava2.common.base.BaseController;
import edu.fridayjava2.common.constant.UserConstants;
import edu.fridayjava2.common.result.RestResult;
import edu.fridayjava2.common.security.service.LoginUser;
import edu.fridayjava2.common.security.service.TokenService;
import edu.fridayjava2.model.SysMenu;
import edu.fridayjava2.model.vo.SysMenuVO;
import edu.fridayjava2.service.SysMenuService;
import edu.fridayjava2.utils.BeanUtils;
import edu.fridayjava2.utils.http.ServletUtils;
import edu.fridayjava2.utils.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/system/menu")
public class SysMenuController extends BaseController {
    @Autowired
    TokenService tokenService;
    @Autowired
    SysMenuService menuService;
    @Autowired
    HttpServletRequest request;
    @RequestMapping("/list")
    public RestResult list(SysMenuVO menu){
        LoginUser loginUser =
                tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();
        List<SysMenu> menus = menuService.selectMenuList(menu, userId);
        return RestResult.success(menus);
    }
    //根据菜单ID获取信息
    @RequestMapping("/{menuId}")
    public RestResult getInfo(@PathVariable Long menuId){
        //调用menuService查询方法
       SysMenu  sysMenu = menuService.selectMenuById(menuId);
       return RestResult.success(sysMenu);
    }
    //新增菜单（用POST请求方式）
    @PostMapping
    public RestResult add(@RequestBody SysMenu menu){
        System.out.println("在add方法中的请求方式为："+request.getMethod());
        //(1)判断用户是否存在，要求用户不存在，数据必须唯一（2）调用新增方法将数据保存到数据库
       // System.out.println("结果"+menuService.checkMenuNameUnique(menu));
        if(UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))){
            return RestResult.error("新增失败，"+menu.getMenuName()+"已经存在");
        }
        return RestResult.success(menuService.insertMenu(menu));
    }
    //修改菜单（用PUT请求方式）
    @PutMapping
    public RestResult edit(@RequestBody SysMenu menu){
        System.out.println("在edit方法中的请求方式为："+request.getMethod());
        if(UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))){
            return RestResult.error("修改失败，"+menu.getMenuName()+"已经存在");
        }
        return RestResult.success(menuService.updateMenu(menu));
    }
    //删除菜单（用DELETE请求方式）
    @DeleteMapping("/{menuId}")
    public RestResult remove(@PathVariable("menuId") Long menuId){
        //首先要判断是否存在子节点，如果没有子节点再去调用删除方法
        if(menuService.hasChildByMenuId(menuId)){
            return RestResult.error("删除失败，存在子节点");
        }
       return toAjax(menuService.deleteMenuById(menuId));
    }
    /**
     * 获取菜单下拉树列表
     */
    @GetMapping("/treeselect")
    public RestResult treeselect(SysMenuVO menu) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();
        List<SysMenu> menus = menuService.selectMenuList(menu, userId);
        List<SysMenuVO> menuVOS = BeanUtils.copyProperties(menus, SysMenuVO.class);
        //System.out.println(menuVOS);
        return RestResult.success(menuService.buildMenuTreeSelect(menuVOS));
    }
    //根据角色Id获取菜单下拉树列表
    @GetMapping(value = "/roleMenuTreeselect/{roleId}")
    public RestResult roleMenuTreeselect(@PathVariable("roleId") Long roleId) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        Long userId = loginUser.getUser().getUserId();
        SecurityUtils.getAuthentication();
        List<SysMenu> menus = menuService.selectMenuList(userId);
        RestResult ajax = RestResult.success();
        //?????????????????到底干啥
        ajax.put("checkedKeys", menuService.selectMenuListByRoleId(roleId));
        List<SysMenuVO> menuVOS = BeanUtils.copyProperties(menus, SysMenuVO.class);
        ajax.put("menus", menuService.buildMenuTreeSelect(menuVOS));
        return ajax;
    }
}
