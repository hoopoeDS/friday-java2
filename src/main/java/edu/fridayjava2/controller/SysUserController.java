package edu.fridayjava2.controller;

import edu.fridayjava2.common.base.BaseController;
import edu.fridayjava2.common.constant.UserConstants;
import edu.fridayjava2.common.result.RestResult;
import edu.fridayjava2.common.result.TableDataInfo;
import edu.fridayjava2.model.SysRole;
import edu.fridayjava2.model.vo.SysUserVO;
import edu.fridayjava2.service.SysRoleService;
import edu.fridayjava2.service.SysUserService;
import edu.fridayjava2.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/system/user")
public class SysUserController extends BaseController {
    @Autowired
    SysUserService sysUserService;
    @Autowired
    SysRoleService sysRoleService;
    //查询所有用户
    @RequestMapping("/list")
    public List<SysUser> findAllUser(){
       return sysUserService.selectAllUser();
    }
    //分页查询用户
    //   int getPageNumber();得到页码，也就是要查询第几页
    //
    //    int getPageSize();得到页面大小，也就是每一页多少数据
    @RequestMapping("/listPage")
    //@GetMapping
    public TableDataInfo list(SysUserVO user, Pageable page){
        int pageNumber = page.getPageNumber()-1;
        pageNumber = pageNumber <=0 ? 0:pageNumber;
        page = PageRequest.of(pageNumber,page.getPageSize());
        return sysUserService.selectUserList(user,page);
    }

    //新增用户
    //@RequestBody,他可以将前端的数据绑定到参数对象中。前端传递的参数名要与对象中的属性名一致。
    @RequestMapping("/add")
    @PostMapping
    public RestResult add(@RequestBody SysUserVO user){
        //需要判断用户名、手机号码、邮箱是否存在
        if(UserConstants.NOT_UNIQUE.equals(sysUserService.checkUserNameUnique(user.getUserName()))){
            return RestResult.error("操作失败，"+user.getUserName()+"已经存在！！！！");
        }else if(UserConstants.NOT_UNIQUE.equals(sysUserService.checkPhoneUnique(user))){
            return RestResult.error("操作失败，"+user.getPhonenumber()+"已经存在！！！！");
        }else if (UserConstants.NOT_UNIQUE.equals(sysUserService.checkEmailUnique(user))){
            return RestResult.error("操作失败，"+user.getEmail()+"已经存在！！！！");
        }
     return toAjax(sysUserService.insertUser(user));
    }

    //删除用户
    @DeleteMapping("/{userIds}")
    public RestResult delete(@PathVariable Long[] userIds){
        int flag = sysUserService.deleteUser(userIds);
        return toAjax(flag);
    }

    //修改用户
    @RequestMapping("/update")
    public RestResult update(@RequestBody SysUserVO user){
        boolean flag = sysUserService.updateUser(user);
        return toAjax(flag?1:0);
    }
    //获取用户信息与角色列表
    @RequestMapping("/{userId}")
    public RestResult getUser(@PathVariable Long userId){
        //(1)通过用户ID得到用户的基本信息
        SysUser user = sysUserService.selectUserByUserId(userId);
        //(2)通过用户ID得到角色信息
        List<SysRole> roles = sysRoleService.selectRoleByUserId(userId);

        String msg = "查询成功";
        RestResult restResult = RestResult.success(msg,user);
        restResult.put("roles",roles);

        return restResult;
    }
}
