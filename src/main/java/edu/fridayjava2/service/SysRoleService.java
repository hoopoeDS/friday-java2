package edu.fridayjava2.service;

import edu.fridayjava2.model.SysRole;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface SysRoleService {
    //通过用户ID查询用户对应的角色信息(指角色的所有信息)
    List<SysRole> selectRoleByUserId(Long userId);
    Set<String> selectRolePermsByUserId(Long userId);
}
