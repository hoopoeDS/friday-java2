package edu.fridayjava2.service.impl;

import edu.fridayjava2.common.constant.UserConstants;
import edu.fridayjava2.common.result.TreeSelect;
import edu.fridayjava2.model.SysMenu;
import edu.fridayjava2.model.vo.MetaVo;
import edu.fridayjava2.model.vo.RouterVo;
import edu.fridayjava2.model.vo.SysMenuVO;
import edu.fridayjava2.model.vo.SysUserVO;
import edu.fridayjava2.repository.SysMenuRepository;
import edu.fridayjava2.service.SysMenuService;
import edu.fridayjava2.utils.BeanUtils;
import edu.fridayjava2.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    SysMenuRepository sysMenuRepository;

    @Override
    public Set<String> selectMenuPermissionByUserId(Long userId) {
        //selectMenuPermissionByUserId（）得到值是List，需要转换成Set
        List<String> perms1 = sysMenuRepository.selectMenuPermissionByUserId(userId);
        HashSet<String> perms1Set = new HashSet<>(perms1);
        return perms1Set ;
    }

    @Override
    public List<SysMenu> selectMenuList(Long userId) {
        return selectMenuList(new SysMenuVO(), userId);
    }

    @Override
    public List<SysMenu> selectMenuList(SysMenuVO menu, Long userId) {
        List<SysMenu> menuList = null;
        SysMenu sysMenu = new SysMenu();
        BeanUtils.copyPropertiesIgnoreEmpty(menu, sysMenu);
        Sort sort = Sort.by("parentId" , "orderNum");
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withMatcher("menuName" ,
                        ExampleMatcher.GenericPropertyMatchers.contains());
        Example example = Example.of(sysMenu, exampleMatcher);
       // 管理员显示所有菜单信息
        if (SysUserVO.isAdmin(userId)) {
            menuList = sysMenuRepository.findAll(example, sort);
        } else {
            menuList = sysMenuRepository.selectMenuListByUserId(sysMenu, userId);
        }
        System.out.println("张三最牛逼");
        return menuList;
    }

    @Override
    public SysMenu selectMenuById(Long menuId) {
        return sysMenuRepository.getById(menuId);
    }
    //插入菜单
    @Override
    public int insertMenu(SysMenu menu) {
        sysMenuRepository.save(menu);
        return (menu!=null&&null!=menu.getMenuId())?1:0;
    }
    //检验菜单的唯一性，通过菜单名来判断
    /**
     * 校验菜单名称是否唯一
     *
     * @param menu 菜单信息
     * @return 结果
     */
    @Override
    public String checkMenuNameUnique(SysMenu menu) {
        Long menuId = StringUtils.isNull(menu.getMenuId()) ? -1L : menu.getMenuId();
        SysMenu sysMenu = new SysMenu();
        //这里有BUG
        //sysMenu.setParentId(menuId);
        sysMenu.setMenuName(menu.getMenuName());
        Page<SysMenu> list = sysMenuRepository.findAll(Example.of(sysMenu), PageRequest.of(0, 1));
        SysMenu info = null;
        if (list.getTotalElements() > 0) {
            info = list.toList().get(0);
        }
        if (StringUtils.isNotNull(info) && info.getMenuId().longValue() != menuId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
    //修改用户
    @Override
    public int updateMenu(SysMenu menu) {
        SysMenu sysMenu = new SysMenu();
        Optional<SysMenu> op = sysMenuRepository.findById(menu.getMenuId());
        if (!op.isPresent()) {
            return 0;//0表示修改失败
        }
        sysMenu = op.get();
        BeanUtils.copyPropertiesIgnoreNull(menu, sysMenu);
        sysMenuRepository.save(sysMenu);

        return 1;//1表示修改成功
    }
    //删除菜单
    //sysMenuRepository.deleteById();
    @Override
    public int deleteMenuById(Long menuId) {
        sysMenuRepository.deleteById(menuId);
        return 1;
    }
    //判断是否存在子节点
    public boolean hasChildByMenuId(Long menuId) {
        SysMenu sysMenu = new SysMenu();
        sysMenu.setParentId(menuId);
        long result = sysMenuRepository.count(Example.of(sysMenu));
        return result > 0 ? true : false;
    }
    /**
     * 构建前端所需要下拉树结构
     *
     * @param menus 菜单列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildMenuTreeSelect(List<SysMenuVO> menus) {
        List<SysMenuVO> menuTrees = buildMenuTree(menus);
        List<TreeSelect> rs = menuTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
        return rs;
    }
    /**
     * 构建前端所需要树结构
     *
     * @param menus 菜单列表
     * @return 树结构列表
     */
    public List<SysMenuVO> buildMenuTree(List<SysMenuVO> menus) {
        List<SysMenuVO> returnList = new ArrayList<SysMenuVO>();
        for (Iterator<SysMenuVO> iterator = menus.iterator(); iterator.hasNext(); ) {
            SysMenuVO t = (SysMenuVO) iterator.next();
            // 根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == 0) {
                recursionFn(menus, t);
                returnList.add(t);
            }
        }
        if (returnList.isEmpty()) {
            returnList = menus;
        }
        return returnList;
    }
    /**
     * 递归列表
     *
     * @param list
     * @param t
     */
    private void recursionFn(List<SysMenuVO> list, SysMenuVO t) {
        // 得到子节点列表
        List<SysMenuVO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysMenuVO tChild : childList) {
            if (hasChild(list, tChild)) {
                // 判断是否有子节点
                Iterator<SysMenuVO> it = childList.iterator();
                while (it.hasNext()) {
                    SysMenuVO n = (SysMenuVO) it.next();
                    recursionFn(list, n);
                }
            }
        }
    }
    /**
     * 得到子节点列表
     */
    private List<SysMenuVO> getChildList(List<SysMenuVO> list, SysMenuVO t) {
        List<SysMenuVO> tlist = new ArrayList<SysMenuVO>();
        Iterator<SysMenuVO> it = list.iterator();
        while (it.hasNext()) {
            SysMenuVO n = (SysMenuVO) it.next();
            if (n.getParentId().longValue() == t.getMenuId().longValue()) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysMenuVO> list, SysMenuVO t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }

    /**
     * 根据角色ID查询菜单树信息
     *
     * @param roleId 角色ID
     * @return 选中菜单列表
     */
    @Override
    public List<Long> selectMenuListByRoleId(Long roleId) {
        List<Long> rs = new ArrayList<>();
        List<SysMenu> menuList = sysMenuRepository.selectMenuListByRoleId(roleId);
        for (SysMenu sysMenu : menuList) {
            rs.add(sysMenu.getMenuId());
        }
        return rs;
    }
    /**
     * 根据用户ID查询菜单
     *
     * @param userId 用户名称
     * @return 菜单列表
     */
    @Override
    public List<SysMenuVO> selectMenuTreeByUserId(Long userId) {
        List<SysMenu> menus = null;
        if (SysUserVO.isAdmin(userId)) {
            menus = sysMenuRepository.selectMenuTreeAll();
        } else {
            menus = sysMenuRepository.selectMenuTreeByUserId(userId);
        }
        List<SysMenuVO> menuVOS = BeanUtils.copyProperties(menus, SysMenuVO.class);
        return getChildPerms(menuVOS, 0);
    }
    /**
     * 根据父节点的ID获取所有子节点
     *
     * @param list     分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<SysMenuVO> getChildPerms(List<SysMenuVO> list, int parentId) {
        List<SysMenuVO> returnList = new ArrayList<SysMenuVO>();
        for (Iterator<SysMenuVO> iterator = list.iterator(); iterator.hasNext(); ) {
            SysMenuVO t = (SysMenuVO) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == parentId) {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }
    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    @Override
    public List<RouterVo> buildMenus(List<SysMenuVO> menus) {
        List<RouterVo> routers = new LinkedList<RouterVo>();
        for (SysMenuVO menu : menus) {
            RouterVo router = new RouterVo();
            router.setName(StringUtils.capitalize(menu.getPath()));
            router.setPath(getRouterPath(menu));
            router.setComponent(StringUtils.isEmpty(menu.getComponent()) ? "Layout" : menu.getComponent());
            router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon()));
            List<SysMenuVO> cMenus = menu.getChildren();
            if (!cMenus.isEmpty() && cMenus.size() > 0 && "M".equals(menu.getMenuType())) {
                router.setAlwaysShow(true);
                router.setRedirect("noRedirect");
                router.setChildren(buildMenus(cMenus));
            }
            routers.add(router);
        }
        return routers;
    }
    /**
     * 获取路由地址
     *
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(SysMenuVO menu) {
        String routerPath = menu.getPath();
        // 非外链并且是一级目录
        if (0 == menu.getParentId() && "1".equals(menu.getIsFrame())) {
            routerPath = "/" + menu.getPath();
        }
        return routerPath;
    }
}
