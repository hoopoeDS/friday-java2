package edu.fridayjava2.service.impl;

import edu.fridayjava2.common.constant.UserConstants;
import edu.fridayjava2.common.result.TableDataInfo;
import edu.fridayjava2.model.vo.SysUserVO;
import edu.fridayjava2.repository.SysUserRepository;
import edu.fridayjava2.service.SysUserService;
import edu.fridayjava2.model.SysUser;
import edu.fridayjava2.utils.BeanUtils;
import edu.fridayjava2.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    SysUserRepository sysUserRepository;

    @Override
    public List<SysUser> selectAllUser() {
        return sysUserRepository.findAll();
    }

    //分页模糊查询所有用户
    @Override
    public TableDataInfo selectUserList(SysUserVO user, Pageable page) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyPropertiesIgnoreEmpty(user,sysUser);
        //delFlag表示删除，为0表示数据没有被删除，为2就表示数据被删除
        sysUser.setDelFlag("0");

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("userName",ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("phonenumber",ExampleMatcher.GenericPropertyMatchers.startsWith());

        Example<SysUser> example = Example.of(sysUser,matcher);
       // sysUser.setParentID=100;
        //Page转化成List,至少用两个方法——getContent()、toList
        Page<SysUser> rs = sysUserRepository.findAll(example,page);//findAll(example,page)的返回值是一个Page
        //得到TableDataInfo，用到public static TableDataInfo success(List<?> list, long total)
        //能不用new关键字就不用
        //toList()把Page转化List，getTotalElements()得到元素数量
        return TableDataInfo.success(rs.toList(),rs.getTotalElements());
    }
    //新增用户有两层含义
    //1、新增用户的信息
    //2、新增用户-角色的信息
    @Override
    @Transactional
    public int insertUser(SysUserVO user) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user,sysUser);
        sysUser.setDelFlag("0");
        System.out.println(sysUser);
        sysUserRepository.save(sysUser);
        System.out.println(user);
        int flag = insertUserRole(user);
        return flag;
    }
     public int insertUserRole(SysUserVO user){
        if(user.getRoleIds().length==0||user.getRoleIds()==null){
            return 0;
        }
        return sysUserRepository.addUserRole(user.getUserId(),user.getRoleIds());
     }

    @Override
    public String checkUserNameUnique(String userName) {
        SysUser sysuser = new SysUser();
        sysuser.setUserName(userName);
        return count(sysuser);
    }
    @Override
    public String count(SysUser sysuser) {
        Example<SysUser> example = Example.of(sysuser);
        //JPA中的count()可以统计满足条件的数据量
        long count = sysUserRepository.count(example);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkPhoneUnique(SysUserVO userInfo) {
        SysUser user = new SysUser();
        BeanUtils.copyProperties(userInfo, user);
        return checkUnique(user);
    }
    @Override
    public String checkEmailUnique(SysUserVO userInfo) {
        SysUser user = new SysUser();
        BeanUtils.copyProperties(userInfo, user);
        return checkUnique(user);
    }
    @Override
    public String checkUnique(SysUser user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        Example<SysUser> example = Example.of(user);
        SysUser info = findOne(example);
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
    //查询用户
    @Override
    public SysUser findOne(Example<SysUser> example) {
        List<SysUser> list = sysUserRepository.findAll(example, PageRequest.of(0, 1)).toList();
        //System.out.println("数据库中的数据个数"+list.size());
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    //删除用户
    @Override
    @Transactional
    public int deleteUser(Long[] userIds) {
        return sysUserRepository.deleteUser(userIds);
    }

    //修改用户
    @Transactional
    @Override
    public boolean updateUser(SysUserVO user) {
        //判断用户是否存在，通过用户ID来判断
      if(!sysUserRepository.existsById(user.getUserId())){
          return false;
      }
          //删除旧的user-role关系
        sysUserRepository.deleteUserRole(user.getUserId());
        //调用新增用户完成修改用户基本信息和新增user—role数据
        int flag = insertUser(user);
        return flag>0;
    }
    //（1）	通过用户ID查询用户的基本信息
    @Override
    public SysUser selectUserByUserId(Long userId) {
        //我们要保证del_flag一定是0，del_flag为2的数据是删除掉的数据，不应该被查询到
        SysUser user = new SysUser();
        user.setUserId(userId);
        user.setDelFlag("0");
        Example<SysUser> example = Example.of(user);
        return sysUserRepository.findOne(example).get();

    }

    //通过用户名查询用户
    @Override
    public SysUser selectUserByUsername(String username) {
        return sysUserRepository.selectUserByUsername(username);
    }
}
