package edu.fridayjava2.service.impl;

import edu.fridayjava2.model.SysRole;
import edu.fridayjava2.repository.SysRoleRepository;
import edu.fridayjava2.service.SysRoleService;
import edu.fridayjava2.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    SysRoleRepository sysRoleRepository;

    @Override
    public Set<String> selectRolePermsByUserId(Long userId) {
       List<SysRole> roleList = sysRoleRepository.selectRolePermsByUserId(userId);
       Set<String> perms = new HashSet<>();
       for(SysRole role:roleList){
          perms.add(role.getRoleKey());
       }

        return perms;
    }
    @Override
    public List<SysRole> selectRoleByUserId(Long userId) {
        return sysRoleRepository.selectRoleByUserId(userId);
    }
}
