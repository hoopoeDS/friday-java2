package edu.fridayjava2.service;

import edu.fridayjava2.common.result.TableDataInfo;
import edu.fridayjava2.model.SysUser;
import edu.fridayjava2.model.vo.SysUserVO;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface SysUserService {
    //查询所有用户
    List<SysUser> selectAllUser();

    //分页查询所有用户
    //还要数据的转化-需要数据层的数据转化成视图层数据（SysUser——SysUserVO）
    TableDataInfo selectUserList(SysUserVO user, Pageable page);

    //新增用户
    int insertUser(SysUserVO user);

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    String checkUserNameUnique(String userName);
    /**
     * 校验用户手机是否唯一
     *
     * @param userInfo 用户信息
     * @return
     */
    String checkPhoneUnique(SysUserVO userInfo);

    /**
     * 校验email是否唯一
     *
     * @param userInfo 用户信息
     * @return
     */
    String checkEmailUnique(SysUserVO userInfo);
    String count(SysUser sysuser);
    String checkUnique(SysUser user);
    SysUser findOne(Example<SysUser> example);

    //删除用户
    int deleteUser(Long[] userIds);

    //修改用户
    boolean updateUser(SysUserVO user);

    //（1）	通过用户ID查询用户的基本信息
    SysUser selectUserByUserId(Long userId);

    //通过用户名查询用户
    SysUser selectUserByUsername(String username);

}
