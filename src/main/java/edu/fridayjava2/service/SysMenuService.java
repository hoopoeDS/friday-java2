package edu.fridayjava2.service;

import edu.fridayjava2.common.result.TreeSelect;
import edu.fridayjava2.model.SysMenu;
import edu.fridayjava2.model.vo.RouterVo;
import edu.fridayjava2.model.vo.SysMenuVO;

import java.util.List;
import java.util.Set;

/**
 * 菜单管理的接口
 */
public interface SysMenuService {
    //通过用户id得到权限信息
    Set<String> selectMenuPermissionByUserId(Long userId);
    //通过用户ID查询菜单列表
    List<SysMenu> selectMenuList(SysMenuVO menu, Long userId);
    List<SysMenu> selectMenuList( Long userId);
    //通过菜单ID查询信息
    SysMenu selectMenuById(Long menuId);
    //判断菜单的唯一性
    String checkMenuNameUnique(SysMenu menu);
    //插入菜单
    int insertMenu(SysMenu menu);
    //修改菜单
    int updateMenu(SysMenu menu);
    //判断是否存在子节点
    boolean hasChildByMenuId(Long menuId);
    //删除菜单
    int deleteMenuById(Long menuId);
    /**
     * 构建前端所需要下拉树结构
     *
     * @param menus 菜单列表
     * @return 下拉树结构列表
     */
    List<TreeSelect> buildMenuTreeSelect(List<SysMenuVO> menus);
    /**
     * 根据角色ID查询菜单树信息
     *
     * @param roleId 角色ID
     * @return 选中菜单列表
     */
    List<Long> selectMenuListByRoleId(Long roleId);
    /**
     * 根据用户ID查询菜单树信息
     *
     * @param userId 用户ID
     * @return 菜单列表
     */
    List<SysMenuVO> selectMenuTreeByUserId(Long userId);
    /**
     * 构建前端路由所需要的菜单
     *
     * @param menus 菜单列表
     * @return 路由列表
     */
    List<RouterVo> buildMenus(List<SysMenuVO> menus);
}
