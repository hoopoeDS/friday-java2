package edu.fridayjava2.config;

import edu.fridayjava2.common.security.filter.JwtAuthenticationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationEntryPoint failHandle;

    @Autowired
    JwtAuthenticationTokenFilter tokenFilter;

    //生成AuthenticationManager对象
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    //认证
    //1、从数据库获取数据的方法  2、设置PasswordEncode对象
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            //userDetailsService("获取用户信息的方法")——//jdbcAuthentication()
            //用户信息不仅仅是用户的基本信息还包含用户的权限信息（或者说是角色信息）
        auth.userDetailsService(userDetailsService)
             //设置编码器
             //BCryptPasswordEncoder 强散列哈希
            .passwordEncoder(new BCryptPasswordEncoder());
    }

    //授权
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //把自定义的过滤器放到认证之前
        http.addFilterBefore(tokenFilter, UsernamePasswordAuthenticationFilter.class);
            //关闭跨域防护
        http.csrf().disable()
                //认证失败的处理,打印失败的信息
                .exceptionHandling().authenticationEntryPoint(failHandle)
                .and()
                //关闭session，后面要用到Token
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                //开启授权
                .authorizeRequests()
                .antMatchers("/login","/profile/avatar/**").anonymous()
                .antMatchers(HttpMethod.GET,"/*.html","/**/*.html","/**/*.css","/**/*.js").permitAll()
                .antMatchers("/profile/**").anonymous()
                .anyRequest().authenticated()
                .and()
                .headers().frameOptions().disable();

    }
}
