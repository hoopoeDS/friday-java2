package edu.fridayjava2.dao;

import edu.fridayjava2.model.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<SysUser,Long> {
    /**
     * 删除用户，用两种方法。
     */
    //方法一：使用JPA提供的方法


    //方法二：使用@Query来实现删除
    @Query("delete from SysUser u where u.userName = ?1 ")
    @Modifying
    void deleteUserByUserName(String userName);

}
