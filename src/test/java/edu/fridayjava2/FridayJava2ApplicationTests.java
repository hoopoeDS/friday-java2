package edu.fridayjava2;

import edu.fridayjava2.dao.UserDao;
import edu.fridayjava2.model.SysRole;
import edu.fridayjava2.model.SysUser;
import edu.fridayjava2.model.vo.SysUserVO;
import edu.fridayjava2.repository.SysRoleRepository;
import edu.fridayjava2.repository.SysUserRepository;
import edu.fridayjava2.service.SysUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class FridayJava2ApplicationTests {

    @Autowired
    SysUserRepository sysUserRepository;
    @Resource
    SysUserService sysUserService;
    @Autowired
    SysRoleRepository sysRoleRepository;

//    @Test
//    void findUser() {
//        SysUser user = userDao.getById(101l);
//        System.out.println(user.getUserName());
//    }
//
//    //方法一：使用JPA提供的方法
//    @Test
//    @Transactional
//    void deleteUser1(){
//        SysUser user = new SysUser();
//        user.setUserName("ds");
//        user.setNickName("hhh");
//        userDao.delete(user);
//    }
//
//    //方法二：使用@Query来实现删除
//    @Test
//    void deleteUser2(){
//        userDao.deleteUserByUserName("ds");
//    }
//    //利用Pageable进行分页查询
//    @Test
//    void dd(){
//        Pageable page = PageRequest.of(0,3);
//
//        Page<SysUser> sysUserPage = userDao.findAll(page);//findAll(page)的返回值是Page<SysUser>
//
//        List<SysUser> users = sysUserPage.getContent();
//        System.out.println(users);
//    }
//    //模糊查询
//    //Example
//    @Test
//    void testExample(){
//        /**
//         * 查询用户名以a字母开头的用户   SysUser-userName
//         */
//        //(1)先得到matcher
//        ExampleMatcher matcher = ExampleMatcher.matching()
//                .withMatcher("userName",ExampleMatcher.GenericPropertyMatchers.startsWith());
//        //(2)再得到具体的查询内容的值，但是值是包含在跟查询结果同一类型的对象里面。
//        SysUser user = new SysUser();
//        user.setUserName("a");
//        //如何得到example
//        //是通过Example.of(T probe, ExampleMatcher matcher)得到
//        //T probe表示具体查询内容,具体就是我们查询结果的一个实体对象
//        //ExampleMatcher matcher表示查询的方式（数据匹配的方式）
//        Example<SysUser> example = Example.of(user, matcher);
//
//        List<SysUser> users = userDao.findAll(example);
//        for(SysUser user1:users){
//            System.out.println(user1.getUserName()+user1.getUserId());
//        }
//    }    @Test
//    void findUser() {
//        SysUser user = userDao.getById(101l);
//        System.out.println(user.getUserName());
//    }
//
//    //方法一：使用JPA提供的方法
//    @Test
//    @Transactional
//    void deleteUser1(){
//        SysUser user = new SysUser();
//        user.setUserName("ds");
//        user.setNickName("hhh");
//        userDao.delete(user);
//    }
//
//    //方法二：使用@Query来实现删除
//    @Test
//    void deleteUser2(){
//        userDao.deleteUserByUserName("ds");
//    }
//    //利用Pageable进行分页查询
//    @Test
//    void dd(){
//        Pageable page = PageRequest.of(0,3);
//
//        Page<SysUser> sysUserPage = userDao.findAll(page);//findAll(page)的返回值是Page<SysUser>
//
//        List<SysUser> users = sysUserPage.getContent();
//        System.out.println(users);
//    }
//    //模糊查询
//    //Example
//    @Test
//    void testExample(){
//        /**
//         * 查询用户名以a字母开头的用户   SysUser-userName
//         */
//        //(1)先得到matcher
//        ExampleMatcher matcher = ExampleMatcher.matching()
//                .withMatcher("userName",ExampleMatcher.GenericPropertyMatchers.startsWith());
//        //(2)再得到具体的查询内容的值，但是值是包含在跟查询结果同一类型的对象里面。
//        SysUser user = new SysUser();
//        user.setUserName("a");
//        //如何得到example
//        //是通过Example.of(T probe, ExampleMatcher matcher)得到
//        //T probe表示具体查询内容,具体就是我们查询结果的一个实体对象
//        //ExampleMatcher matcher表示查询的方式（数据匹配的方式）
//        Example<SysUser> example = Example.of(user, matcher);
//
//        List<SysUser> users = userDao.findAll(example);
//        for(SysUser user1:users){
//            System.out.println(user1.getUserName()+user1.getUserId());
//        }
//    }
    //测试AddUserRole
    @Test
    void testAddUserRole(){
        sysUserRepository.addUserRole(200l,new Long[]{100l,101l,102l});

    }
    //测试新增用户业务层和数据层
    @Test
    void add(){
        SysUserVO sysUserVO = new SysUserVO();
        sysUserVO.setUserId(555l);
        sysUserVO.setUserName("xiao");
        sysUserVO.setNickName("x");
        sysUserVO.setRoleIds(new Long[]{100l,101l,102l,103l});
        sysUserService.insertUser(sysUserVO);
    }

    //测试删除用户
    @Test
    void testdeleteUser(){
        sysUserService.deleteUser(new Long[]{100l,101l});

    }

    @Test
    void testselectUserByUserId(){
       System.out.println(sysUserService.selectUserByUserId(102l));
    }
    @Test
    void testselectRoleByUserId(){
        List<SysRole> roles = sysRoleRepository.selectRoleByUserId(100l);
        for(SysRole role : roles){
            System.out.println(role);
        }
    }
    @Test
    void testselectUserByUsername(){
        System.out.println(sysUserService.selectUserByUsername("元春"));
    }

}
